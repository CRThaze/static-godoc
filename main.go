package main

import (
	"bytes"
	"container/list"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"strings"

	"golang.org/x/net/html"

	"github.com/anaskhan96/soup"
	walk "github.com/karrick/godirwalk"
)

// HashTable is a set implemented with a hashmap holding empty structs.
type HashTable map[string]struct{}

// Add adds an element to the HashTable.
func (h HashTable) Add(val string) {
	h[val] = struct{}{}
}

// Has returns true if the HashTable contains a string, and false otherwise.
func (h HashTable) Has(val string) bool {
	if _, ok := h[val]; !ok {
		return false
	}
	return true
}

// ToSlice returns the HashTable as a slice of strings.
func (h HashTable) ToSlice() []string {
	result := make([]string, len(h))
	var i uint
	for val := range h {
		result[i] = val
		i++
	}
	return result
}

const (
	exitSuccess uint8 = iota
	exitGenericError
	exitBadArgs
	exitCouldNotScrape
)

var (
	cleanupTasks = list.New()
	filesWritten = HashTable{}
)

func registerCleanUpTask(task func()) {
	cleanupTasks.PushBack(task)
}

func cleanExit(code ...uint8) {
	for e := cleanupTasks.Front(); e != nil; e = e.Next() {
		e.Value.(func())()
	}
	var exitCode uint8
	if len(code) == 0 {
		exitCode = exitSuccess
	} else {
		exitCode = code[0]
	}
	os.Exit(int(exitCode))
}

var args struct {
	pkgName      string
	goPath       string
	outputPath   string
	pkgRoot      string
	godocCmd     string
	verbose      bool
	symlinks     bool
	exclude      string
	excludedDirs HashTable
}

func initAndValidateArgs() {
	flag.StringVar(
		&args.pkgName,
		"pkg-name",
		"",
		"The name of the package for which documentation should be generated."+
			"If none is provided, we will attempt to derive it using the GOPATH.",
	)
	flag.StringVar(
		&args.goPath,
		"gopath",
		os.Getenv("GOPATH"),
		"The GOPATH value to use instead of $GOPATH.",
	)
	flag.StringVar(
		&args.outputPath,
		"output",
		"godocs",
		"The path where the documentation files should be written. "+
			"It will be created if it does not exist.",
	)
	flag.StringVar(
		&args.pkgRoot,
		"root",
		os.Getenv("PWD"),
		"The root of the package for which documentation should be generated. "+
			"If none is provided, the current directory will be used.",
	)
	flag.StringVar(
		&args.godocCmd,
		"godoc-cmd",
		"godoc",
		"The executable to use for generating go docs.",
	)
	flag.BoolVar(
		&args.verbose,
		"v",
		false,
		"Whether to print verbose output.",
	)
	flag.BoolVar(
		&args.symlinks,
		"symlinks",
		false,
		"Whether to follow symlinks when generating documentation.",
	)
	flag.StringVar(
		&args.exclude,
		"exclude",
		"vendor,testdata",
		"Comma-delimited list of directory names which should be excluded.",
	)
	flag.Parse()

	// Sanitize.
	args.pkgRoot = strings.TrimSuffix(args.pkgRoot, "/")
	args.goPath = strings.TrimSuffix(args.goPath, "/")
	args.outputPath = strings.TrimSuffix(args.outputPath, "/")
	args.excludedDirs = HashTable{}
	for _, dir := range strings.Split(args.exclude, ",") {
		args.excludedDirs.Add(dir)
	}
	if !strings.HasPrefix(args.outputPath, "/") {
		args.outputPath = args.pkgRoot + "/" + args.outputPath
	}

	if args.pkgName == "" && args.goPath != "" {
		abs, err := filepath.Abs(args.pkgRoot)
		if err != nil {
			fmt.Printf("ERROR: could not find absolute path of -root: %v\n", err)
			cleanExit(exitBadArgs)
		}
		args.pkgRoot = abs
		goPathPkgPrefixLen := len(args.goPath + "/src/")
		if len(args.pkgRoot) > goPathPkgPrefixLen {
			args.pkgName = args.pkgRoot[goPathPkgPrefixLen:]
		}
	}

	validationResults := map[string]bool{
		"-pkg-name must be provided if not under GOPATH/src directory":     args.pkgName != "",
		"-gopath must be provided if $GOPATH environment variable not set": args.goPath != "",
		"-root must be under the GOPATH": strings.HasPrefix(
			args.pkgRoot, args.goPath,
		),
		"-godoc-cmd must exist": func() bool {
			if _, err := exec.LookPath(args.godocCmd); err != nil {
				return false
			}
			return true
		}(),
	}

	for msg, valid := range validationResults {
		if !valid {
			fmt.Printf("ERROR: %s.\n", msg)
			flag.Usage()
			cleanExit(exitBadArgs)
		}
	}
}

func verboseLogf(format string, v ...interface{}) {
	if args.verbose {
		fmt.Printf(format+"\n", v...)
	}
}

func verboseLog(v ...interface{}) {
	if args.verbose {
		fmt.Println(v...)
	}
}

type pkgInfo struct {
	isGoPkg bool
	subPkgs HashTable
	doc     []byte
}

var pkgs = map[string]*pkgInfo{}

func extractNode(node *soup.Root) {
	if node.Pointer == nil {
		return
	}
	node.Pointer.Parent.RemoveChild(node.Pointer)
}

func removeAttr(node *soup.Root, key string) {
	for i, attr := range node.Pointer.Attr {
		if attr.Key == key {
			node.Pointer.Attr = append(node.Pointer.Attr[:i], node.Pointer.Attr[i+1:]...)
		}
	}
}

func updateAttr(node *soup.Root, key string, val string) {
	for i, attr := range node.Pointer.Attr {
		if attr.Key == key {
			node.Pointer.Attr[i].Val = val
		}
	}
}

func modifyHTML(doc string, pkgPath string, subPkgs HashTable) ([]byte, error) {
	tagSoup := soup.HTMLParse(doc)

	// Prepend golang.org to all the links in the <head> tag.
	for _, link := range tagSoup.Find("head").FindAll("link") {
		attrs := link.Attrs()
		updateAttr(&link, "href", "https://golang.org"+attrs["href"])
	}

	// Remove the topbar and footer.
	topbar := tagSoup.FindStrict("div", "id", "topbar")
	extractNode(&topbar)
	footer := tagSoup.FindStrict("div", "id", "footer")
	extractNode(&footer)

	// Add index.html to all the pkg-dir links.
	pkgDir := tagSoup.FindStrict("div", "class", "pkg-dir")
	if pkgDir.Pointer != nil {
		twoDots := pkgDir.FindStrict("a", "href", "..")
		extractNode(&twoDots)
		for _, a := range pkgDir.FindAll("a") {
			attrs := a.Attrs()
			updateAttr(&a, "href", attrs["href"]+"index.html")
		}
	}

	// Delete all pkg-name links which are not valid subpackages.
	for _, td := range tagSoup.FindAllStrict("td", "class", "pkg-name") {
		linkTarget := fmt.Sprint(args.outputPath, "/", td.Find("a").Attrs()["href"])
		if !filesWritten.Has(linkTarget) {
			extractNode(&td)
		}
	}

	// Fix the main body links.
	page := tagSoup.Find("div", "id", "page")
	if page.Pointer != nil {
		for _, a := range page.FindAll("a") {
			attrs := a.Attrs()

			// Ensure all subpackage links are relative.
			re, err := regexp.Compile(
				fmt.Sprintf(
					"/pkg/%s/.*(%s)/(.*)",
					args.pkgName, strings.Join(subPkgs.ToSlice(), "|"),
				),
			)
			if err != nil {
				return []byte{}, err
			}
			updateAttr(&a, "href", re.ReplaceAllString(attrs["href"], "$1/index.html$2"))

			// Make all remaining /pkg or /doc links point to godoc.og
			if strings.HasPrefix(attrs["href"], "/pkg/") || strings.HasPrefix(attrs["href"], "/doc/") {
				updateAttr(&a, "href", "https://godoc.org"+attrs["href"])
			} else if strings.HasPrefix(attrs["href"], "/src/") { // Disable all /src links.
				removeAttr(&a, "href")
			}
		}
	}

	// Render full modified document and return.
	var buf bytes.Buffer
	w := io.Writer(&buf)
	html.Render(w, tagSoup.Pointer)
	return buf.Bytes(), nil
}

func runGoDoc(url string) ([]byte, error) {
	verboseLog(url)
	out, err := exec.Command(args.godocCmd, "-url", url).Output()
	if err != nil {
		return []byte{}, err
	}
	return out, nil
}

func writePkgDoc(pkgPath string, subPkgs HashTable) error {
	pkgName := strings.TrimPrefix(pkgPath, args.goPath+"/src/")
	doc, err := runGoDoc("/pkg/" + pkgName)
	if err != nil {
		return err
	}
	correctedHTML, err := modifyHTML(string(doc), pkgPath, subPkgs)
	if err != nil {
		return err
	}
	outputFile := fmt.Sprint(
		args.outputPath+"/",
		strings.TrimPrefix(
			fmt.Sprint(
				strings.TrimPrefix(pkgPath, args.pkgRoot),
				"/index.html",
			),
			"/",
		),
	)
	verboseLogf("Writing %s documentation to %s", pkgName, outputFile)
	os.MkdirAll(filepath.Dir(outputFile), os.ModePerm)
	if err := ioutil.WriteFile(
		outputFile,
		correctedHTML,
		0644,
	); err != nil {
		return err
	}
	// Record that we have written this file (for later filtering of package links.)
	filesWritten.Add(outputFile)
	return nil
}

func genDocs() error {
	// Walk the Package Root.
	verboseLogf("Walking %s...", args.pkgRoot)
	return walk.Walk(
		args.pkgRoot,
		&walk.Options{
			FollowSymbolicLinks: args.symlinks,
			// Define callback to run for every entity found.
			Callback: func(path string, de *walk.Dirent) error {
				verboseLogf("Examining %s", path)
				if de.IsDir() {
					// Skip excluded and hidden directories.
					if args.excludedDirs.Has(de.Name()) || strings.HasPrefix(de.Name(), ".") {
						return filepath.SkipDir
					}
					// Add newly found directory to the map of potential packages.
					pkgs[path] = &pkgInfo{subPkgs: HashTable{}}
					return nil
				}
				dirName := filepath.Dir(path)
				// If we have already determined this directory is a Go package we don't need to do more.
				if pkgs[dirName].isGoPkg {
					return nil
				}
				// Check if this file ends in .go.
				// If it does then the directory it is in is a Go package and it should be added to its
				// parents list of sub-packages.
				if len(de.Name()) > 3 && de.Name()[len(de.Name())-3:] == ".go" {
					p := pkgs[dirName]
					p.isGoPkg = true
					verboseLogf("%s is a Go package.", dirName)
					parentName := filepath.Dir(dirName)
					if parent, ok := pkgs[parentName]; ok {
						parent.subPkgs.Add(filepath.Base(dirName))
					}
				}
				return nil
			},
			// Define callback to run after processing all children for a directory.
			PostChildrenCallback: func(path string, de *walk.Dirent) error {
				// Skip packages that don't contain go files or sub packages.
				if !(pkgs[path].isGoPkg) && len(pkgs[path].subPkgs.ToSlice()) == 0 {
					return nil
				}
				verboseLogf("Generating Documentation for %s", path)
				return writePkgDoc(path, pkgs[path].subPkgs)
			},
			// Define callback for handling errors.
			ErrorCallback: func(path string, err error) walk.ErrorAction {
				fmt.Printf("ERROR: Failed while processing '%s': %v", path, err)
				return walk.Halt
			},
		},
	)
}

func main() {
	initAndValidateArgs()

	if os.Getenv("GOROOT") == "" {
		os.Setenv("GOROOT", args.goPath)
	}

	cwd := os.Getenv("PWD")
	if err := os.Chdir(args.pkgRoot); err != nil {
		fmt.Printf("Could not access the provided -root directory: %v\n", err)
		cleanExit(exitGenericError)
	}
	defer os.Chdir(cwd)

	if err := genDocs(); err != nil {
		fmt.Printf("There were errors generating documentation!: %v\n", err)
		cleanExit(exitGenericError)
	}
}
