# static-godoc

**static-godoc** is a utility for generating static GoDoc pages, using and including
only locally available files.  The resulting pages can then be used to host
limited documentation anywhere you choose, or it can be commited to the project
repository for offline viewing.

## Installation

```
go get gitlab.com/CRThaze/static-godoc
```

## Usage

Running the following in the root of your Go project will generate static
documentation pages under `./godocs/`

```
static-godoc -v
```

To see full usage run:

```
static-godoc -h
```

## Building

**static-godoc** can be build from under a standard `$GOPATH` as usual, with
`go build`. Or if you prefer to build it outside of a `$GOPATH` you can do so
as well using `make`.

Inside `$GOPATH`:

```
dep ensure
go build
```

Outside `$GOPATH`:

```
make vendor
make
```
