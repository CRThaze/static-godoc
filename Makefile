PACKAGE  = gitlab.com/CRThaze/static-godoc
GOPATH   = $(CURDIR)/.gopath
BASE     = $(GOPATH)/src/$(PACKAGE)
BIN    = $(GOPATH)/bin
GOLINT = $(BIN)/golint
GODOC = $(BIN)/godoc

PKGS = $(or $(PKG), $(shell cd $(BASE) && \
	env GOPATH=$(GOPATH) $(GO) list ./... | grep -v "^$(PACKAGE)/vendor/"))


GO    = go
DEP = dep

.DEFAULT_GOAL := all

$(BASE):
	@mkdir -p $(dir $@)
	@ln -sf $(CURDIR) $@

.PHONY: all
all: | $(BASE)
	cd $(BASE) && $(GO) build -o bin/$(PACKAGE) main.go

.PHONY: vendor-init
vendor-init: | $(BASE)
	cd $(BASE) && $(DEP) init

.PHONY: vendor-add
vendor-add: | $(BASE)
	cd $(BASE) && $(DEP) ensure -add ${ADD}

.PHONY: vendor
vendor: | $(BASE)
	cd $(BASE) && $(DEP) ensure

$(BIN)/golint: | $(BASE)
	cd $(BASE) && $(GO) get golang.org/x/lint/golint

.PHONY: docs
docs: | $(BASE)
	cd $(BASE) && bin/$(PACKAGE) -v -symlinks -exclude vendor,.gopath

.PHONY: vet
vet: | $(BASE)
	@cd $(BASE) && ret=0 && for pkg in $(PKGS); do \
		test -z "$$($(GO) vet $$pkg | tee /dev/stderr)" || ret=1 ; \
		done ; exit $$ret

.PHONY: lint
lint: vendor | $(BASE) $(GOLINT)
	@cd $(BASE) && ret=0 && for pkg in $(PKGS); do \
		test -z "$$($(GOLINT) $$pkg | tee /dev/stderr)" || ret=1 ; \
		done ; exit $$ret

TIMEOUT = 20
TEST_TARGETS := test-default test-bench test-short test-verbose test-race
.PHONY: $(TEST_TARGETS) check test tests
test-bench:   ARGS=-run=__absolutelynothing__ -bench=.
test-short:   ARGS=-short
test-verbose: ARGS=-v
test-race:    ARGS=-race
$(TEST_TARGETS): test

check test tests: lint | $(BASE)
	@cd $(BASE) && $(GO) test -timeout $(TIMEOUT)s $(ARGS) ./...

